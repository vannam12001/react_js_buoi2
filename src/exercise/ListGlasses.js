import React, { Component } from "react";
import ItemGlasses from "./ItemGlasses";
export default class ListGlasses extends Component {
  renderListGlasses() {
    return this.props.dataGlassesArr.map((glasses, index) => {
      return (
        <ItemGlasses
          data={glasses}
          key={index}
          handleChangeGlasses={this.props.handleChangeGlasses}
        />
      );
    });
  }
  render() {
    return <div className="row">{this.renderListGlasses()}</div>;
  }
}
