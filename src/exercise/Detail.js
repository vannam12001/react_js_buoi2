import React, { Component } from "react";

export default class Detail extends Component {
  render() {
    let { url, price, name, desc } = this.props.detail;
    return (
      <div className="item_change" style={{ position: "absolute" }}>
        <img src={url} alt="" />
        <div className="text-detail text-left">
          <h3>Name: {name}</h3>
          <p>Price: {price}</p>
          <p>Desc: {desc}</p>
        </div>
      </div>
    );
  }
}
