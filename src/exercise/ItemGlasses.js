import React, { Component } from "react";

export default class ItemGlasses extends Component {
  render() {
    let { data, handleChangeGlasses } = this.props;
    let { url } = data;
    return (
      <div className="col-3 text-center p-4">
        <button
          style={{ border: "none" }}
          onClick={() => {
            handleChangeGlasses(data);
          }}
        >
          <img className="item_glasses" src={url} alt="" />
        </button>
      </div>
    );
  }
}
