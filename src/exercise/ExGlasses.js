import React, { Component } from "react";
import { dataGlassesArr } from "./data";
import ListGlasses from "./ListGlasses";
import Detail from "./Detail";
export default class Ex_glasses extends Component {
  state = {
    dataGlassesArr,
    detail: dataGlassesArr[0],
  };
  handleChangeGlasses = (glasses) => {
    this.setState({
      detail: glasses,
    });
  };
  render() {
    return (
      <div id="exercise">
        <div className="container">
          <h1 className="text-center p-5">Exercise React JS Buổi 31</h1>
          <div className="row pb-5">
            <div className="col-6 text-center">
              <h1>Before</h1>
              <img src="./img/glassesImage/model.jpg" alt="" />
            </div>
            <div className="col-6 text-center">
              <h1>After</h1>
              <Detail detail={this.state.detail} />
              <img src="./img/glassesImage/model.jpg" alt="" />
            </div>
          </div>
          <h1 className="text-center p-3">Change color glasses</h1>
          <ListGlasses
            handleChangeGlasses={this.handleChangeGlasses}
            dataGlassesArr={this.state.dataGlassesArr}
          />
        </div>
      </div>
    );
  }
}
